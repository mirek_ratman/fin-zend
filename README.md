# Zend Framework instance

This repo contains instance of Zend Framework where I've included Certificate class functionality.
As you can see Ive used ZF-MVC version 1.x and I want to show you few reasons why:
- for high performace website (backend functionality which will deliver data) is necessary to have
  fast backend which is able to get as much as it can responses for requests. unfortunatelly ZF2
  is very slow in comparison to ZF1. You cna find many comparisons where you can find that ZF2 is slower sometimes 10 times.
  Talking about details please see one of many comparison here: http://www.developerknowhow.com/1637/zf1-vs-zf2
- I will go more and will say even ZF1 is not the best choice in this case. For simple websites is better even use
  some simple MVC framework like FatFREE (around 250 req/sec) or Phalcon (around 800 req/sec) which is done as a extension
  library written in C. Some info here:  http://systemsarchitect.net/performance-benchmark-of-popular-php-frameworks/
- Dependency in ZF2 is deeper than in ZF1 - here is quite good comparison of how dependency looks in many frameworks
  http://phalcontutorial.blogspot.com/2013/10/benchmark-reports-on-c-based-framework.html and again comparison.
- Quite interesting thing is a graph which is showing number of function calls in frameworks showing that ZF2 has
  around 5600 functiona calls for just simple "Hello word" which is hudge amount in comparison to example Yii (around 250)

# How to show data

Framework is configured to support multidomain environment which can be a benefit for small apps.
There is only simple configuration needed to do to run many websites in separate MVC environment.
Ive used local domain http://FIN.LOCAL for test. You can use same name or different but then you need to reconfigure framework.

# Domain reconfiguration

a) In framework config file /application/config/config.xml you can change the <fin.local> field name to different domain name
example <finapp.local> or create a copy/paste it with different name. And thats all. Everything is working.
You can imediately see result on http://finapp.local/certificate

b) to run the page with React/flux functionality on http://finapp.local/certificate-graph is also extremely easy
to do by changing:
```
apiDomain: 'http://finapp.local',
```
in /src/frontend/flux/config/Config.js and rebuilt output bu running GULP config gulpfile.js from main REPO path.


## displayAsHtml()

You can display a simple page calling
```
http://fin.local/certificate
```
where you can find simple page with data from Certificate class and
its childrens (BonusCertificate,GuaranteeCertificate).

## displayAsXml()

You can display a simple page calling
```
http://fin.local/certificate/xml
```
where you can find simple XML file with data from Certificate class and
its children (BonusCertificate).

There is no properties from GuaranteeCertificate included in to XML what was a part of task.
Maybe I didnt catch exactly the sens of the sentence "Additionaly, please take care that GuaranteeCertificate musn't be exported...."
sentence but I've decided to just simply not "print" properties from GuaranteeCertificate.
I can add extra notification also. That i strongly suggest to define this type of things before "job" will be done.

