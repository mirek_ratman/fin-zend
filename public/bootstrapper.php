<?php
define("_APP_ACCESS", true);
$pExceptions = null;
$defaultPaths = array('./', '../library', '../application/library');
set_include_path(join(PATH_SEPARATOR, $defaultPaths) . PATH_SEPARATOR . get_include_path());
require_once('../vendor/autoload.php');
date_default_timezone_set('Europe/Prague');
require_once('Zend/Loader/Autoloader.php');
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Main_');
Zend_Session::start();


/*********************************************************************/
//Multidomain support
/*********************************************************************/
function getCustomEnv($key)
{
    $prefix = "REDIRECT_";
    if (array_key_exists($key, $_SERVER))
        return $_SERVER[$key];
    foreach ($_SERVER as $k => $v) {
        if (substr($k, 0, strlen($prefix)) == $prefix) {
            if (substr($k, -(strlen($key))) == $key)
                return $v;
        }
    }
    return null;
}

Zend_Registry::set('domain', getCustomEnv('sys_domain'));
Zend_Registry::set('subdomain', getCustomEnv('sys_subdomain'));
Zend_Registry::set('host', (Zend_Registry::get('subdomain') != '' ? Zend_Registry::get('subdomain') . '.' : '') . Zend_Registry::get('domain'));
$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
Zend_Registry::set('rootUrl', $protocol . Zend_Registry::get('host'));

$pMainConfig = new Zend_Config_Xml('../application/config/config.xml');
$pMainRoutesFile = '../application/config/routes.xml';
$currentDomain = Zend_Registry::get('host');

if (isset($pMainConfig->domains->$currentDomain)) {
    Zend_Registry::set('appEnv', $pMainConfig->domains->$currentDomain->domainEnv);
    if (isset($pMainConfig->domains->$currentDomain->globalConfig)) {
        $config = new Zend_Config(array(), true);
        $config->merge(new Zend_Config_Xml($pMainConfig->domains->$currentDomain->globalConfig, Zend_Registry::get('appEnv')));
        $config->merge(new Zend_Config_Xml($pMainConfig->domains->$currentDomain->config, Zend_Registry::get('appEnv')));
    } else {
        $config = new Zend_Config_Xml($pMainConfig->domains->$currentDomain->config, Zend_Registry::get('appEnv'));
    }

    Zend_Registry::set('config', $config);

    //Specific namespaces
    if (isset($config->namespaces)) {
        foreach ($config->namespaces as $namespace) {
            if (isset($namespace)) {
                $autoloader->registerNamespace($namespace);
            }
        }
    }

    $router = new Zend_Controller_Router_Rewrite();
    $router->removeDefaultRoutes();
    if (file_exists($pMainRoutesFile)) {
        $router->addConfig(new Zend_Config_Xml($pMainRoutesFile, Zend_Registry::get('appEnv')), 'routes');
    }
    $router->addConfig(new Zend_Config_Xml($config->dirs->config . $config->routes, Zend_Registry::get('appEnv')), 'routes');

    $frontController = Zend_Controller_Front::getInstance();
    $frontController->setBaseUrl('/');
    $frontController->setRouter($router);
    $frontController->setParam('noErrorHandler', !$config->errorHandler);
    $frontController->throwExceptions($config->throwExceptions);
    $frontController->registerPlugin(new Main_Controller_Plugin_SetupController());
    $frontController->registerPlugin(new Main_Controller_Plugin_SetupView());

    Zend_Registry::set('language', $config->defaultLanguage);
} else {
    Throw New Exception ('No config defined for domain ' . $currentDomain);
    die();
}

if (Zend_Registry::get('appEnv') === 'development') {
    error_reporting(E_ALL | E_STRICT);
}

try {
    if ($config->compressOutput) {
        $frontController->returnResponse(true);
        $response = $frontController->dispatch();

        $search = array('/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s');
        $replace = array('>', '<', '\\1');

        die(preg_replace($search, $replace, $response->getBody()));
    } else {
        $frontController->dispatch();
    }
} catch (Exception $e) {
    if (Zend_Registry::get('appEnv') === 'development' || Zend_Registry::get('appEnv') === 'stage') {
        print_r($e);
    }

    if (Zend_Registry::get('appEnv') === 'production') {

        if (isset($_GET['debugmode']) && $_GET['debugmode'] = 'true') {
            debug($e);
        }

        $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        $redirector->gotoRoute(
            array(),
            'error-exception'
        );
    }
}
