<div class="boxCertificate">
    <dl tal:condition="php: certificate->getIsin()">
        <dd><h2>ISIN</h2></dd>
        <dd>${php: certificate->getIsin()}</dd>
    </dl>
    <dl tal:condition="php: certificate->getTradingMarket()">
        <dd><h4>Trading Market</h4></dd>
        <dd>
            <dl class="list">
                <dd>MIC</dd>
                <dd>${php: certificate->getTradingMarket()['mic']}</dd>
            </dl>
            <dl class="list">
                <dd>Name</dd>
                <dd>${php: certificate->getTradingMarket()['name']}</dd>
            </dl>
        </dd>
    </dl>
    <dl tal:condition="php: certificate->getCurrency()">
        <dd><h4>Currency</h4></dd>
        <dd>${php: certificate->getCurrency()}</dd>
    </dl>
    <dl tal:condition="php: certificate->getIssuer()">
        <dd><h4>Issuer</h4></dd>
        <dd>${php: certificate->getIssuer()}</dd>
    </dl>
    <dl tal:condition="php: certificate->getIssuingPrice()">
        <dd><h4>Issuing price</h4></dd>
        <dd>${php: certificate->getIssuingPrice()}</dd>
    </dl>
    <dl tal:condition="php: certificate->getBarier()">
        <dd><h4>Barier</h4></dd>
        <dd>${php: certificate->getBarier()}</dd>
    </dl>
    <dl tal:condition="php: certificate->getParticipationRate()">
        <dd><h4>Participation rate</h4></dd>
        <dd>${php: certificate->getParticipationRate()}</dd>
    </dl>
    <dl tal:condition="php: certificate->getDocuments()->getDocuments()">
        <dd><h4>Documents</h4></dd>
        <dd>
            <dl class="list" tal:repeat="el php: certificate->getDocuments()->getDocuments()">
                <dd>${el/file}</dd>
                <dd>${el/typeName}</dd>
            </dl>
        </dd>
    </dl>
    <dl tal:condition="php: certificate->getPriceHistory()->getPriceHistory()">
        <dd><h4>Prices history</h4></dd>
        <dd>
            <dl class="list" tal:repeat="el php: certificate->getPriceHistory()->getPriceHistory()">
                <dd>${php: date('Y-m-d H:i:s',el['timestamp'])}</dd>
                <dd>${el/price}</dd>
            </dl>
        </dd>
    </dl>

</div>
