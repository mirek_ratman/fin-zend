<?xml version="1.0"?>
<certificate>
    <isin>${php: certificate->getIsin()}</isin>
    <tradingMarket>
        <mic>${php: certificate->getTradingMarket()['mic']}</mic>
        <name>${php: certificate->getTradingMarket()['name']}</name>
    </tradingMarket>
    <currency>${php: certificate->getCurrency()}</currency>
    <issuer>${php: certificate->getIssuer()}</issuer>
    <issuingPrice>${php: certificate->getIssuingPrice()}</issuingPrice>
    <documents>
        <tal:block tal:repeat="el php: certificate->getDocuments()->getDocuments()">
            <document>
                <file>${el/file}</file>
                <type>${el/type}</type>
                <typeName>${el/typeName}</typeName>
            </document>
        </tal:block>
    </documents>
    <priceHistory>
        <tal:block tal:repeat="el php: certificate->getPriceHistory()->getPriceHistory()">
            <price>
                <timestamp>${el/timestamp}</timestamp>
                <val>${el/price}</val>
            </price>
        </tal:block>
    </priceHistory>
    <barier>${php: certificate->getBarier()}</barier>
</certificate>