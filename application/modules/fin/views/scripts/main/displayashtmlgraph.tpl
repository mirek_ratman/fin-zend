<div class="row">
    <div class="col-sm-4">
        <div class="tile-stats tile-red">
            <div class="num">${php: certificate->getIsin()}</div>
            <h3>ISIN</h3>
            <p>Trading Market: ${php: strtoupper(certificate->getTradingMarket()['mic'])}, ${php: certificate->getTradingMarket()['name']}</p>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="tile-stats tile-green">
            <div class="num">${php: certificate->getIssuingPrice()}</div>
            <h3>Issuing price</h3>
            <p>Currency: ${php: certificate->getCurrency()}, Barier: ${php: certificate->getBarier()}</p>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="tile-stats tile-aqua">
            <div class="num">${php: certificate->getIssuer()}</div>
            <h3>Issuer</h3>
            <p>Participation rate: ${php: certificate->getParticipationRate()}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4>
                        Real Time Price History
                    </h4>
                </div>
            </div>
            <div id="reactGraph" class="panel-body no-padding"></div>
        </div>

    </div>
</div>
