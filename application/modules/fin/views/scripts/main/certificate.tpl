<div>
    <h2>${php: certificate->getIsin()}</h2>
    <div id="container" style="height: 400px; min-width: 310px"></div>
    <script tal:condition="libHighcharts | false" src="/js/graph.js"></script>

</div>