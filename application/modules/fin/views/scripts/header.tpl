﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"></meta>
        <title>${headTitle | null}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>

        <!-- Libraries -->
        <script tal:condition="libJquery | false" src="http://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript"></script>
        <link tal:condition="libBootstrap | false" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" type="text/css" media="screen"></link>
        <script tal:condition="libBootstrap | false" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
        <script tal:condition="libGraph | false" src="/lib/highstock/js/highstock.js"></script>
        <script tal:condition="libGraph | false" src="/lib/highstock/js/modules/exporting.js"></script>
        <link tal:condition="libGraph | false" rel="stylesheet" href="/css/neon-core.css" />
        <link tal:condition="libGraph | false" rel="stylesheet" href="/css/neon-theme.css" />
        <link tal:condition="libGraph | false" rel="stylesheet" href="/css/neon-forms.css" />
        <link tal:condition="libGraph | false" rel="stylesheet" href="/lib/rickshaw/rickshaw.min.css" />
        <script tal:condition="libGraph | false"  src="/lib/rickshaw/vendor/d3.v3.js"></script>
        <script tal:condition="libGraph | false"  src="/lib/rickshaw/rickshaw.min.js"></script>

        <!-- styles -->
        <link rel="stylesheet" href="/css/style.css" type="text/css" media="screen"></link>
        <!-- javascript -->

    </head>
    <body class="page-body">
        <div class="page-container">

