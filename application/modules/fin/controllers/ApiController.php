<?php
/**
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Mirek Ratman
 * @version 1.0
 * @since 2015-01-14
 */
defined("_APP_ACCESS") or die();

class Fin_ApiController extends Creo_Controller_Action
{
    /*
     * Certificate class
     * @var array
     */
    private $certificate;

    /**
     * @return mixed
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * @param mixed $certificate
     */
    public function setCertificate()
    {
        $this->certificate = new Fin\GuaranteeCertificate();
        $this->certificate->setIsin('US0378331005');
        $this->certificate->setTradingMarket('xxsc');
        $this->certificate->setCurrency('EUR');
        $this->certificate->setIssuer('Apple');
        $this->certificate->setIssuingPrice(23.30);
        $this->certificate->getDocuments()->addDocument('document1.pdf', 0);
        $this->certificate->getDocuments()->addDocument('document1.doc', 1);
        $this->certificate->updatePricesHistory('http://ichart.finance.yahoo.com/table.csv?s=AAPL&a=0&b=01&c=2011&d=11&e=31&f=2011&g=d');

        $this->certificate->setBarier(23.30);
        $this->certificate->setParticipationRate('50%');
    }

    /**
     * Get Price History Action
     */
    public function getpricehistoryAction()
    {
        self::appDefaults(false);
        self::setCertificate();

        //Because graph is working with X,Y keys is necessary to prepare the content right way.
        // This step can be skipped when we wil define the source different way
        $out = array_map(function($tag) {
            return array(
                'x' => (int)( $tag['timestamp'] + floor(time()/10000000) ),
                'y' => (double)number_format($tag['price'] + rand($tag['price']-100, $tag['price']+100))
            );
        }, array_reverse($this->getCertificate()->getPriceHistory()->getPriceHistoryAsArray()));

        echo json_encode($out);
        header ("Content-Type:text/json");
    }

}
