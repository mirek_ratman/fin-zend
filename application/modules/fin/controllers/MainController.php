<?php
/**
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Mirek Ratman
 * @version 1.0
 * @since 2015-01-14
 */
defined("_APP_ACCESS") or die();

class Fin_MainController extends Creo_Controller_Action
{
    /*
     * Certificate class
     * @var array
     */
    private $certificate;

    /**
     * @return mixed
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * @param mixed $certificate
     */
    public function setCertificate()
    {
        $this->certificate = new Fin\GuaranteeCertificate();
        $this->certificate->setIsin('US0378331005');
        $this->certificate->setTradingMarket('xxsc');
        $this->certificate->setCurrency('EUR');
        $this->certificate->setIssuer('Apple');
        $this->certificate->setIssuingPrice(23.30);
        $this->certificate->getDocuments()->addDocument('document1.pdf',0);
        $this->certificate->getDocuments()->addDocument('document1.doc',1);
        $this->certificate->updatePricesHistory('http://ichart.finance.yahoo.com/table.csv?s=AAPL&a=0&b=01&c=2011&d=11&e=31&f=2011&g=d');

        $this->certificate->setBarier(23.30);
        $this->certificate->setParticipationRate('50%');
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        parent::appDefaults();

        $this->view->meta = array('title' => 'Fin.local');
    }

    /**
     * displayAsHtml Action
     */
    public function displayashtmlAction()
    {
        parent::appDefaults();
        self::setCertificate();

        $this->view->certificate = $this->getCertificate();
    }

    /**
     * displayAsHtml Action
     */
    public function displayashtmlgraphAction()
    {
        parent::appDefaults();
        self::setCertificate();

        $this->view->libJquery = true;
        $this->view->libBootstrap = true;
        $this->view->libGraph = true;

        $this->view->certificate = $this->getCertificate();
    }

    /**
     * displayAsXml Action
     */
    public function displayasxmlAction()
    {
        self::setCertificate();

        $this->view->certificate = $this->getCertificate();
        header ("Content-Type:text/xml");
    }

}
