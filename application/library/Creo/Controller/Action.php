<?php

/**
 * class Creo_Controller_Action
 * Default action controller
 */
class Creo_Controller_Action extends Zend_Controller_Action
{

    /**
     * Load defaults for this application
     * @param mixed $initView
     * @return mixed
     */
    public function appDefaults($initView = 'all')
    {

        $config = Zend_Registry::get('config');

        //Set app default varibles
        $this->view->appEnv = Zend_Registry::get('appEnv');
        $this->view->appDomain = Zend_Registry::get('domain');
        $this->view->appSubdomain = Zend_Registry::get('subdomain');
        $this->view->appHost = Zend_Registry::get('host');
        $this->view->appModule = $this->getRequest()->getModuleName();
        $this->view->appController = $this->getRequest()->getControllerName();
        $this->view->appAction = $this->getRequest()->getActionName();

        //Set default registry entries
        Zend_Registry::set('authSession', null);
        Zend_Registry::set('rightsSession', null);

        switch ($initView) {
            case 'all':
                $headerFooter = new Main_View_Headerfooter();
                $headerFooter->setHeader(Zend_Registry::get('viewPath') . '/header.tpl');
                $headerFooter->setFooter(Zend_Registry::get('viewPath') . '/footer.tpl');

                parent::initView();
                $this->view->getEngine()->setPreFilter($headerFooter);
                $this->view->addHelperPath('Main/Helper/', 'Main_Helper');
                $this->view->view = $this->view;
                $this->view->meta = array('title' => Zend_Registry::get('domain'));
                break;
            case false:
                $this->_helper->viewRenderer->setNoRender(true);
                break;
            case 'no-header-footer':
                parent::initView();
                $this->view->view = $this->view;
                $this->view->meta = array('title' => Zend_Registry::get('domain'));
                break;
        }
    }

    /**
     * Add jQuery support
     */
    public function addJquery()
    {
        $this->view->libJquery = true;
    }

    /**
     * Add Twitter Bootstrap support
     */
    public function addBootstrap()
    {
        $this->view->libBootstrap = true;
    }

}
