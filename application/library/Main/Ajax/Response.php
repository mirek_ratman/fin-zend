<?php
/**
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Miroslaw Ratman
 * @version 1.0
 * @since 2015-04-03
 */
defined("_APP_ACCESS") or die();


class Main_Ajax_Response
{
    public $data;
    public $code;
    public $message;


    /**
     * Class constructor
     * @method __construct
     * @return mixed
     */
    public function __construct()
    {
        $this->data = null;
        $this->code = 0;
        $this->message = '';
    }

}