<?php
/**
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Miroslaw Ratman
 * @version 1.0
 * @since 2015-02-03
 */
defined("_APP_ACCESS") or die();


class Main_Ajax
{
    /**
     * Verify if request is ajax call from current domain
     * @method isAjax
     * @return boolean
     */
    public function isAjax()
    {
        return (
        isset($_SERVER['HTTP_X_REQUESTED_WITH'])
        && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
        && $_SERVER['sys_domain'] == Zend_Registry::get('domain')
            ? true : (Zend_Registry::get('appEnv') == 'production' ? false : true)
        );
    }


    /**
     * Make default schema for ajax response
     * @method makeResponse
     * @param bolean $response
     * @param bolean $charset - set character encoding for response data
     * @param bolean $cache - enable response cache
     * @param int $expire - expiration time
     * @param int $hash - header etag hash
     * @return json
     */
    public function makeResponse($code = '404', $data = null, $message = 'Error', $responseType = null, $charset = 'utf-8', $cache = false, $expire = 0, $hash = null)
    {
        //Definition
        $out = new Main_Ajax_Response();

        //Run
        $out->code = $code;
        $out->data = $data;
        $out->message = $message;

        //Check if response type was given, else add default value from config
        if ($responseType === null) {
            $responseType = Zend_Registry::get('config')->ajax->defaultResponseType;
        }

        //Set response type
        switch ($responseType) {
            case 'xml':
                $response = self::encodeXML($out);
                break;
            case 'json':
                $response = self::encodeJSON($out);
                break;

            default:
                $response = null;
                break;
        }

        //Set headers
        self::setHeaders($code, $responseType, $charset, $cache, $expire, $hash);

        //return response
        return $response;
    }


    /**
     * Prepare headers for ajax response
     * @method setHeaders
     * @param string $type - type of data will be send
     * @param bolean $charset - set character encoding for response data
     * @param bolean $cache - enable response cache
     * @param int $expire - expiration time
     * @param int $hash - header etag hash
     */
    private function setHeaders($code = 404, $type, $charset = 'utf-8', $cache = false, $expire = 0, $hash = null)
    {
        //Cache headers
        if (Zend_Registry::get('config')->cache->ajax->globalRefresh == 'false' && $cache == 'true') {
            $currTime = gmmktime();
            $expire = $expire + $currTime;

            header("Cache-Control: maxage=" . $expire);
            header("Expires: " . gmdate("r", $expire));
            header("Last-Modified: " . gmdate("r", $currTime));
            header("Etag: " . $hash);
            if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $currTime || trim($_SERVER['HTTP_IF_NONE_MATCH']) == $hash) {
                header("HTTP/1.1 304 Not Modified");
                die();
            }
        } else {
            header("Cache-Control: no-cache, must-revalidate");
            header("Expires: Sat, 26 Jul 1997 00:00:00 GMT");
        }

        //Response type headers
        switch ($type) {
            case 'xhtml':
                header('Content-type: text/xhtml; charset=' . $charset);
                break;
            case 'html':
                header('Content-type: text/html; charset=' . $charset);
                break;
            case 'xml':
                header('Content-type: text/xml; charset=' . $charset);
                break;
            case 'json':
                header('Content-type: application/json; charset=' . $charset);
                break;

            default:
                header('Content-type: text/html; charset=' . $charset);
                break;
        }

        //Response code
        switch ($code) {
            case 404:
                header("HTTP/1.0 404 Not Found");
                break;
            case 200:
                header("HTTP/1.0 200 OK");
                break;
            case 204:
                header("HTTP/1.0 204 No Content");
                break;
            case 500:
                header("HTTP/1.0 500 Internal Server Error");
                break;

            default:
                header("HTTP/1.0 500 Internal Server Error");
                break;
        }

    }


    /**
     * Metod will kill request
     * @method killRequest
     * @response false
     */
    public function killRequest()
    {
        die(header("HTTP/1.0 404 Not Found"));
    }


    /**
     * encode data object to JSON
     * @method encodeJson
     * @param mixed $obj - object to encode
     * @response json
     */
    private function encodeJSON($obj)
    {
        return json_encode($obj);
    }


    /**
     * encode data object to XML
     * @method encodeXml
     * @param mixed $obj - object to encode
     * @response xml
     */
    private function encodeXML($obj)
    {
        //Prepare XML header
        $out = '<?xml version="1.0"?>';

        //Encode XML

        return $out;
    }

}