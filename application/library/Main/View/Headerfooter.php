<?php
/**
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Miroslaw Ratman
 * @version 1.0
 * @since 2015-03-10
 */
defined("_APP_ACCESS") or die(header('HTTP/1.0 404 Not Found'));


class Main_View_HeaderFooter implements PHPTAL_Filter
{

    private $header = '';
    private $footer = '';
    private $viewPath = '';

// initialize class
    public function __construct()
    {
    }

//public functions
    public function getFooter()
    {
        return $this->footer;
    }

    public function getHeader()
    {
        return $this->header;
    }

    public function setViewPath($viewPath)
    {
        $this->viewPath = $viewPath;
    }

    public function setFooter($footer)
    {
        $this->footer = $this->viewPath . $footer;
        return $this;
    }

    public function setHeader($header)
    {
        $this->header = $this->viewPath . $header;
        return $this;
    }

    public function filter($str)
    {
        if (!empty($this->header)) {
            $str = file_get_contents($this->header) . $str;
        }

        if (!empty($this->footer)) {
            $str .= file_get_contents($this->footer);
        }

        return $str;
    }
}
