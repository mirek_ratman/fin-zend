<?php
/**
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Miroslaw Ratman
 * @version 1.0
 * @since 2015-03-10
 */
defined("_APP_ACCESS") or die(header('HTTP/1.0 404 Not Found'));


class Main_Controller_Plugin_SetupController extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        $frontController = Zend_Controller_Front::getInstance();
        $config = Zend_Registry::get('config');

        $frontController->setDefaultControllerName($config->defaultController);
        $frontController->setDefaultAction($config->defaultAction);

        if ($config->dirs->controllers == null) {
            Throw new Exception('No "controllers" dir specified');
        }
        $frontController->setControllerDirectory($config->dirs->controllers->toArray());

        //Set default controller dir
        $frontController->addControllerDirectory('../application/modules/main/controllers/', 'main');
    }
}