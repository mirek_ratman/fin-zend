<?php
/**
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Miroslaw Ratman
 * @version 1.0
 * @since 2015-03-10
 */
defined("_APP_ACCESS") or die(header('HTTP/1.0 404 Not Found'));


class Main_Controller_Plugin_SetupView extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        $config = Zend_Registry::get('config');
        $module = $request->module;

        Zend_Registry::set('viewPath', $config->dirs->views->$module ? $config->dirs->views->$module : $config->dirs->views->toArray());

        $viewConfig['scriptPath'] = Zend_Registry::get('viewPath');
        $viewConfig['encoding'] = $config->phptal->charset;

        $view = new Main_View_Phptalview($viewConfig);
        if (!is_dir($config->phptal->compiledDir)) {
            mkdir($config->phptal->compiledDir, 0744);
            chown($config->phptal->compiledDir, get_current_user());
        }
        $view->getEngine()->setPhpCodeDestination($config->phptal->compiledDir);
        $view->getEngine()->setPhpCodeExtension($config->phptal->ext);
        $view->getEngine()->setForceReparse($config->phptal->forceReparse);

        if (isset($config->helpers)) {
            foreach ($config->helpers as $helper) {
                $view->addHelperPath($helper->directory, $helper->namespace);
            }
        }

        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer($view);
        $viewRenderer->setViewSuffix($config->viewSuffix);

        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
    }
}