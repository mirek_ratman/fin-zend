<?php
/**
 * Helper for parameters
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Miroslaw Ratman
 * @version 1.0
 * #since 2015-03-25
 */
defined("_APP_ACCESS") or die(header("HTTP/1.0 404 Not Found"));


class Main_Helper_Params
{
    /**
     * @var $params
     */
    private $params;

    /**
     * Class constructor
     * @method __construct
     * @param string $params - object with GET or POST params
     */
    public function __construct($params = array())
    {
        $this->params = $params;
    }

    /**
     * Method will check if param exists in given GET or POST array and return value
     * @method getParam
     * @param [string] $name - param name to take
     * @param [string] $reg - regular expression to validate parameter
     * @param [mixed] $alternative - alternative value if checked param not exists or its null
     * @return string
     */
    public function getParam($name, $reg = null, $alternative = null)
    {
        $param = isset($this->params[$name]) && $this->params[$name] !== null && $this->params[$name] !== '' ? $this->params[$name] : ($alternative !== null ? $alternative : '');
        if ($reg !== null) {
            return self::validate($param) ? $param : null;
        } else {
            return $param;
        }
    }


    /**
     * Method will validate given value base on delivered regular expression
     * @method applyReg
     * @param [string] $val - value to check
     * @param [string] $reg - regular expression to validate value
     * @return [boolean]
     */
    private function validate($val, $reg)
    {
        $validator = new Zend_Validate_Regex(array('pattern' => $reg));
        if ($validator->isValid($val)) {
            return true;
        } else {
            return false;
        }
    }


}
