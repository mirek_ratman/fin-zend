<?php
/**
 * Helper for parameters
 * @copyright Copyright (c) Miroslaw Ratman
 * @author Miroslaw Ratman
 * @version 1.0
 * #since 2015-01-25
 */
defined("_APP_ACCESS") or die(header("HTTP/1.0 404 Not Found"));


class Main_Helper_Datetime
{
    /**
     * Method will return date-time string
     * @method getDatetime
     * @return string
     */
    public static function getCurrentDatetime()
    {
        return date('Y-m-d H:i:s');
    }
}
