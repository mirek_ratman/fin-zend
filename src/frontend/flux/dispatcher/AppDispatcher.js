/**
 * Created by miroslawratman on 04/08/15.
 */
'use strict';

var Dispatcher = require('flux').Dispatcher;
var Assign = require('object-assign');
var Api = require('../services/Api');


var AppDispatcher = Assign(new Dispatcher(), {
    handleGetPriceHistoryAction: function (next) {
        Api.getPriceHistory(function (res) {
            next(res);
        });
    }
});

module.exports = AppDispatcher;
