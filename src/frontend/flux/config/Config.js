/**
 * Created by miroslawratman on 04/08/15.
 */
'use strict';

var Config = {
    apiDomain: 'http://finapp.local',
    apiPort: '80'
};

/**
 * Config module exports
 * @return [object]
 */
module.exports = Config;
