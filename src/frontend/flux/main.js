/** @jsx React.DOM */
/**
 * Created by miroslawratman on 04/08/15.
 */
'use strict';

var React = require('react');
var App = require('./components/App.js');

React.render(
  <App />,
  document.getElementById('reactGraph')
);
