/**
 * Created by miroslawratman on 04/08/15.
 */
'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');

var AppActions = {
    getPriceHistory: function (next) {
        AppDispatcher.handleGetPriceHistoryAction(next);
    }
}

module.exports = AppActions
