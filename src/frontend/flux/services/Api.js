/**
 * Created by miroslawratman on 04/08/15.
 */
'use strict';

var Rp = require('request-promise');
var Config = require('../config/Config');

/**
 * Wrapper for calling a API
 */
var Api = {
    getPriceHistory: function (next) {
        var requestOpts = {
            encoding: 'utf8',
            uri: Config.apiDomain + ':' + Config.apiPort + '/api/get-price-history',
            method: 'GET',
        };

        Rp(requestOpts)
            .then(function (data) {
                next(data);
            })
            .catch(function (err) {
                console.log(err);
            });
    }
};

module.exports = Api;