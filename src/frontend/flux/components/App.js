/** @jsx React.DOM */
/**
 * Created by miroslawratman on 04/08/15.
 */
'use strict';

var React = require('react');
var AppActions = require('../actions/AppActions');

var App = React.createClass({
    componentDidMount: function () {
        var graph = new Rickshaw.Graph({
            element: document.getElementById("rickshaw-chart-demo"),
            height: 193,
            renderer: 'area',
            stroke: false,
            preserve: true,
            series: [{
                color: '#73c8ff',
                data: this.getInitialState()['data'],
                name: 'Price history'
            }]
        });

        graph.render();

        var hoverDetail = new Rickshaw.Graph.HoverDetail({
            graph: graph,
            xFormatter: function (x) {
                return new Date(x * 10).toString();
            }
        });

        var legend = new Rickshaw.Graph.Legend({
            graph: graph,
            element: document.getElementById('rickshaw-legend')
        });

        var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
            graph: graph,
            legend: legend
        });

        setInterval(function () {
            AppActions.getPriceHistory(function (data) {
                graph.series[0].data = JSON.parse(data);
                graph.update();
            });
        }, 500);
    },
    getInitialState: function () {
        return {
            data: [{x: 0, y: 0}]
        }
    },
    render: function () {
        return (
            <div id="rickshaw-chart-demo">
                <div id="rickshaw-legend" />
            </div>
        )
    }
});

module.exports = App;
